/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.projectpub.drive_in._enum.EProductType;
import com.projectpub.drive_in.dao.OrderDaoJpa;
import com.projectpub.drive_in.dao.ProductDaoJpa;
import com.projectpub.drive_in.domain.Order;
import com.projectpub.drive_in.domain.Product;
import java.util.ArrayList;
import javax.persistence.EntityManager;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Jip
 */
@RunWith(MockitoJUnitRunner.class)
public class ProductDaoJpaTest {

    @Mock
    EntityManager entityManagerMock;

    @InjectMocks
    ProductDaoJpa productDaoJpaMock;

    @Test
    public void create() {
        Product tempProduct = new Product(0, "Coca Cola", 125, EProductType.DRINK);
        productDaoJpaMock.create(tempProduct);
        verify(entityManagerMock).persist(tempProduct);
        verify(entityManagerMock).flush();
    }

    @Test
    public void update() {
        Product tempProduct = new Product(0, "Coca Cola", 125, EProductType.DRINK);
        productDaoJpaMock.update(tempProduct);
        verify(entityManagerMock).merge(tempProduct);
        verify(entityManagerMock).flush();
    }

    @Test
    public void find() {
        productDaoJpaMock.findByInternalId(0);
        verify(entityManagerMock).find(Product.class, 0L);
    }
}
