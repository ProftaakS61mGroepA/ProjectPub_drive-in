/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.projectpub.drive_in.dao.OrderDaoJpa;
import com.projectpub.drive_in.domain.Order;
import java.util.ArrayList;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Jip
 */
@RunWith(MockitoJUnitRunner.class)
public class OrderDaoJpaTest {

    @Mock
    EntityManager entityManagerMock;

    @InjectMocks
    OrderDaoJpa orderDaoJpaMock;

    @Test
    public void create() {
        Order tempOrder = new Order(0, new ArrayList<>());
        orderDaoJpaMock.create(tempOrder);
        verify(entityManagerMock).persist(tempOrder);
        verify(entityManagerMock).flush();
    }

    @Test
    public void update() {
        Order tempOrder = new Order(0, new ArrayList<>());
        orderDaoJpaMock.update(tempOrder);
        verify(entityManagerMock).merge(tempOrder);
        verify(entityManagerMock).flush();
    }

    @Test
    public void find() {
        orderDaoJpaMock.findByInternalId(0);
        verify(entityManagerMock).find(Order.class, 0L);
    }
}
