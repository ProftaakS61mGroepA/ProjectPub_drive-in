package controller;

import com.google.gson.GsonBuilder;
import com.projectpub.drive_in.Application;
import com.projectpub.drive_in._enum.EOrderState;
import com.projectpub.drive_in._interface.IOrderService;
import com.projectpub.drive_in.controller.OrderController;
import com.projectpub.drive_in.domain.Order;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration(classes = Application.class)
public class OrderControllerTest {

    @Autowired
    private MockMvc mvc;

    @Mock
    IOrderService orderServiceInstanceMock;

    @InjectMocks
    OrderController OrderControllerMock;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(OrderControllerMock).build();
    }

    @Test
    public void get() throws Exception {
        List<Order> orders = new ArrayList<>();
        orders.add(new Order(0, new ArrayList<>()));
        orders.add(new Order(1, new ArrayList<>()));
        when(orderServiceInstanceMock.findAll()).thenReturn(orders);

        mvc.perform(MockMvcRequestBuilders
                .get("/drive-in/orders"))
                .andExpect(status().isOk())
                .andExpect(content().json(new GsonBuilder().create().toJson(orders)));

        verify(orderServiceInstanceMock).findAll();
    }

    @Test
    public void create() throws Exception {
        Order order = new Order(1, new ArrayList<>());
        String orderContent = new GsonBuilder().create().toJson(order);

        when(orderServiceInstanceMock.create(order.getId(), order.getProducts()))
                .thenReturn(order);

        mvc.perform(MockMvcRequestBuilders
                .post("/drive-in/order")
                .contentType(MediaType.APPLICATION_JSON).content(orderContent))
                .andExpect(status().isOk())
                .andExpect(content().json(orderContent));

        verify(orderServiceInstanceMock).create(order.getId(), order.getProducts());
    }

    @Test
    public void update() throws Exception {
        Order order1 = new Order(1, new ArrayList<>());
        Order order2 = new Order(1, new ArrayList<>());
        order2.setOrderState(EOrderState.CREATED);
        String order2Content = new GsonBuilder().create().toJson(order2);

        when(orderServiceInstanceMock.update(order2.getInternalId(),
                order2.getId(),
                order2.getProducts(),
                order2.getOrderState()))
                .thenReturn(order2);

        mvc.perform(MockMvcRequestBuilders
                .put("/drive-in/order")
                .contentType(MediaType.APPLICATION_JSON)
                .content(order2Content))
                .andExpect(status().isOk())
                .andExpect(content().json(order2Content));

        verify(orderServiceInstanceMock).update(order2.getInternalId(),
                order2.getId(),
                order2.getProducts(),
                order2.getOrderState());
    }

    @Test
    public void onOrderStatusChange() throws Exception {
        Order order1 = new Order(1, new ArrayList<>());
        Order order2 = new Order(1, new ArrayList<>());
        order2.setOrderState(EOrderState.CREATED);
        String order2Content = new GsonBuilder().create().toJson(order2);

        when(orderServiceInstanceMock.onOrderStatusChange(order2.getId(),order2.getOrderState()))
                .thenReturn(order2);

        mvc.perform(MockMvcRequestBuilders
                .put("/drive-in/onorderstatuschange")
                .contentType(MediaType.APPLICATION_JSON)
                .content(order2Content))
                .andExpect(status().isOk())
                .andExpect(content().json(order2Content));
        
        verify(orderServiceInstanceMock)
                .onOrderStatusChange(order2.getId(),order2.getOrderState());
    }
}
