package controller;

import com.google.gson.GsonBuilder;
import com.projectpub.drive_in.Application;
import com.projectpub.drive_in._enum.EProductType;
import com.projectpub.drive_in._interface.IProductService;
import com.projectpub.drive_in.controller.ProductController;
import com.projectpub.drive_in.domain.Product;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration(classes = Application.class)
public class ProductControllerTest {

    @Autowired
    private MockMvc mvc;

    @Mock
    IProductService productServiceInstanceMock;

    @InjectMocks
    ProductController ProductControllerMock;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(ProductControllerMock).build();
    }

    @Test
    public void get() throws Exception {
        List<Product> products = new ArrayList<>();
        products.add(new Product(0, "Cola", 120, EProductType.DRINK));
        products.add(new Product(1, "Fries", 240, EProductType.FOOD));
        when(productServiceInstanceMock.findAll()).thenReturn(products);

        mvc.perform(MockMvcRequestBuilders
                .get("/drive-in/products"))
                .andExpect(status().isOk())
                .andExpect(content().json(new GsonBuilder().create().toJson(products)));

        verify(productServiceInstanceMock).findAll();
    }

    @Test
    public void create() throws Exception {
        Product product = new Product(0, "Cola", 120, EProductType.DRINK);
        String productContent = new GsonBuilder().create().toJson(product);

        when(productServiceInstanceMock.create(product.getId(),
                product.getName(),
                product.getPriceInCents(),
                product.getCategory()))
                .thenReturn(product);
        
        mvc.perform(MockMvcRequestBuilders
                .post("/drive-in/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content(productContent))
                .andExpect(status().isOk())
                .andExpect(content().json(productContent));

        verify(productServiceInstanceMock)
                .create(product.getId(), product.getName(), product.getPriceInCents(), product.getCategory());
    }

    @Test
    public void update() throws Exception {
        Product product2 = new Product(0, "Fanta", 312, EProductType.DRINK);
        String product2Content = new GsonBuilder().create().toJson(product2);

        when(productServiceInstanceMock.update(product2.getInternalId(),
                product2.getId(),
                product2.getName(),
                product2.getPriceInCents(),
                product2.getCategory()))
                .thenReturn(product2);
        
        mvc.perform(MockMvcRequestBuilders
                .put("/drive-in/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content(product2Content))
                .andExpect(status().isOk())
                .andExpect(content().json(product2Content));

        verify(productServiceInstanceMock)
                .update(product2.getInternalId(),
                        product2.getId(),
                        product2.getName(),
                        product2.getPriceInCents(),
                        product2.getCategory());
    }
}
