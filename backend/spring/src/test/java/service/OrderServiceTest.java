/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.projectpub.drive_in._enum.EOrderState;
import com.projectpub.drive_in._enum.EOrigin;
import com.projectpub.drive_in._enum.EProductType;
import com.projectpub.drive_in._interface.IOrderDao;
import dao.*;
import com.projectpub.drive_in.dao.OrderDaoJpa;
import com.projectpub.drive_in.domain.Order;
import com.projectpub.drive_in.domain.Product;
import com.projectpub.drive_in.service.OrderService;
import java.util.ArrayList;
import javax.persistence.EntityManager;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Jip
 */
@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {

    @Mock
    IOrderDao orderDaoInstanceMock;

    @InjectMocks
    OrderService orderServiceMock;

    @Test(expected = IllegalArgumentException.class)
    public void createNullOrder() {
        orderServiceMock.create(0, null);
    }

    @Test
    public void createOrder() {
        when(orderDaoInstanceMock.create(any(Order.class))).then(returnsFirstArg());
        Order tempOrder = orderServiceMock.create(10, new ArrayList<>());
        verify(orderDaoInstanceMock).create(tempOrder);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNullProduct() {
        orderServiceMock.update(0, 0, null, EOrderState.UNKNOWN);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNullState() {
        orderServiceMock.update(0, 0, new ArrayList<>(), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNonExistingOrder() {
        when(orderDaoInstanceMock.findByInternalId(0)).thenReturn(null);
        orderServiceMock.update(0, 10, new ArrayList<>(), EOrderState.UNKNOWN);
    }

    @Test
    public void updateOrder() {
        Order alreadyCreated = new Order(10, new ArrayList<>());
        alreadyCreated.setInternalId(0);
        when(orderDaoInstanceMock.findByInternalId(0)).thenReturn(alreadyCreated);

        Order NewOrder = new Order(11, new ArrayList<>());
        NewOrder.setInternalId(0);
        NewOrder.setOrderState(EOrderState.CREATED);

        ArrayList<Product> products = new ArrayList<>();
        products.add(new Product(0, "Cola", 100, EProductType.DRINK));
        NewOrder.setProducts(products);

        orderDaoInstanceMock.update(NewOrder);
        verify(orderDaoInstanceMock).update(NewOrder);
    }

    @Test
    public void findOrder() {
        Order alreadyCreated = new Order(10, new ArrayList<>());
        alreadyCreated.setInternalId(0);
        when(orderDaoInstanceMock.findByInternalId(0)).thenReturn(alreadyCreated);

        assertEquals(orderServiceMock.findByInternalId(0), alreadyCreated);
    }
}
