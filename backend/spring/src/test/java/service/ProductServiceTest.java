/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.projectpub.drive_in._enum.EOrderState;
import com.projectpub.drive_in._enum.EOrigin;
import com.projectpub.drive_in._enum.EProductType;
import com.projectpub.drive_in._interface.IOrderDao;
import com.projectpub.drive_in._interface.IProductDao;
import dao.*;
import com.projectpub.drive_in.dao.OrderDaoJpa;
import com.projectpub.drive_in.domain.Order;
import com.projectpub.drive_in.domain.Product;
import com.projectpub.drive_in.service.OrderService;
import com.projectpub.drive_in.service.ProductService;
import java.util.ArrayList;
import javax.persistence.EntityManager;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Jip
 */
@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    @Mock
    IProductDao productDaoInstanceMock;

    @InjectMocks
    ProductService productServiceMock;

    @Test(expected = IllegalArgumentException.class)
    public void createNullProductType() {
        productServiceMock.create(0, "Cola", 100, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createEmptyProductName() {
        productServiceMock.create(0, "", 100, EProductType.DRINK);
    }

    @Test
    public void createProduct() {
        when(productDaoInstanceMock.create(any(Product.class))).then(returnsFirstArg());
        Product tempProduct = productServiceMock.create(0, "Cola", 100, EProductType.DRINK);
        verify(productDaoInstanceMock).create(tempProduct);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNullName() {
        productServiceMock.update(0, 0, "", 100, EProductType.DRINK);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNullType() {
        productServiceMock.update(0, 0, "Cola", 100, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNegativePrice() {
        productServiceMock.update(0, 0, "Cola", -100, EProductType.DRINK);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNonExistingProduct() {
        when(productDaoInstanceMock.findByInternalId(0)).thenReturn(null);
        productServiceMock.update(0, 10, "Cola", 100, EProductType.FOOD);
    }

    @Test
    public void updateProduct() {
        Product alreadyCreated = new Product(10, "Cola", 100, EProductType.FOOD);
        alreadyCreated.setInternalId(0);
        when(productDaoInstanceMock.findByInternalId(0)).thenReturn(alreadyCreated);

        Product NewProduct = new Product(11, "Fries", 100, EProductType.FOOD);
        NewProduct.setInternalId(0);

        productDaoInstanceMock.update(NewProduct);
        verify(productDaoInstanceMock).update(NewProduct);
    }

    @Test
    public void findProduct() {
        Product alreadyCreated = new Product(11, "Fries", 100, EProductType.FOOD);
        alreadyCreated.setInternalId(0);
        when(productDaoInstanceMock.findByInternalId(0)).thenReturn(alreadyCreated);

        assertEquals(productServiceMock.findByInternalId(0), alreadyCreated);
    }
}
