/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.drive_in.domain;

import com.projectpub.drive_in._enum.EOrderState;
import com.projectpub.drive_in._enum.EOrigin;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Jip
 */
@Entity
@Table(name = "_Order")
public class Order implements Serializable {

    @Id
    @GeneratedValue
    private long internalId;
    @Column(unique = true)
    private long id;
    private EOrderState orderState;
    private EOrigin origin;
    @ManyToMany
    private List<Product> products;

    public Order() {
    }

    public Order(long externalId, ArrayList<Product> products) {
        this.id = externalId;
        this.orderState = EOrderState.UNKNOWN;
        this.origin = EOrigin.DRIVEIN;
        this.products = products;
    }

    public ArrayList<Product> getProducts() {
        return new ArrayList<>(products);
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    public EOrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(EOrderState orderState) {
        this.orderState = orderState;
    }

    public long getInternalId() {
        return internalId;
    }

    public void setInternalId(long internalId) {
        this.internalId = internalId;
    }

    public EOrigin getOrigin() {
        return origin;
    }

    public void setOrigin(EOrigin origin) {
        this.origin = origin;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return super.toString() + " internalId: " + this.internalId + " externalId: " + this.id + " orderState: " + this.orderState.toString() + " origin: " + this.origin.toString() + " products: " + this.products.toString(); //To change body of generated methods, choose Tools | Templates.
    }
}
