package com.projectpub.drive_in;

import com.projectpub.drive_in.controller.OrderController;
import com.projectpub.drive_in.dao.OrderDaoJpa;
import com.projectpub.drive_in.dao.ProductDaoJpa;
import com.projectpub.drive_in.service.OrderService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(Application.class, args);
    }
}
