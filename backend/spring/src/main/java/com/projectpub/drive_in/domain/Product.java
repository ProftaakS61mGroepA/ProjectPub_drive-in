/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.drive_in.domain;

import com.projectpub.drive_in._enum.EProductType;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

/**
 *
 * @author Jip
 */
@Entity
public class Product {

    @Id
    @GeneratedValue
    private long internalId;
    @Column(unique = true)
    private long id;
    private String name;
    private long priceInCents;
    private EProductType category;

    public Product() {
    }

    public Product(long externalId, String name, long priceInCents, EProductType productType) {
        this.id = externalId;
        this.name = name;
        this.priceInCents = priceInCents;
        this.category = productType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPriceInCents() {
        return priceInCents;
    }

    public void setPriceInCents(long priceInCents) {
        this.priceInCents = priceInCents;
    }

    public EProductType getCategory() {
        return category;
    }

    public void setCategory(EProductType category) {
        this.category = category;
    }

    public long getInternalId() {
        return internalId;
    }

    public void setInternalId(long internalId) {
        this.internalId = internalId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
