/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.drive_in._enum;

/**
 *
 * @author Jip
 */
public enum EOrderState {
    UNKNOWN,//Order created on drive-in backend
    CREATED,//Order received by ordering backend, id assigned
    MAKEABLE,//Order in queue for bar/kitchen
    MAKING,//Kitchen/bar working on order
    DELIVERABLE,//Order can be delivered
    DELIVERING,//Order is being delivered
    DELIVERED,//Order is delivered
    COMPLETED,//Order is paid and done
    ABORTED;//Order is aborted before payment
}
