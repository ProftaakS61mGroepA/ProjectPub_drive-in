/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.drive_in.dao;

import com.projectpub.drive_in._interface.IOrderDao;
import com.projectpub.drive_in.domain.Order;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 *
 * @author Jip
 */
@Repository
public class OrderDaoJpa implements IOrderDao {

    @PersistenceContext
    EntityManager em;

    @Override
    public Order create(Order order) {
        em.persist(order);
        em.flush();
        em.refresh(order);
        return order;
    }

    @Override
    public Order update(Order order) {
        em.merge(order);
        em.flush();
        em.refresh(order);
        return order;
    }

    @Override
    public Order findByInternalId(long internalId) {
        return em.find(Order.class, internalId);
    }

    @Override
    public Order findByExternalId(long externalId) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Order> query = cb.createQuery(Order.class);
        Root<Order> order = query.from(Order.class);

        CriteriaQuery<Order> orders = query.select(order).where(cb.equal(order.get("id"), externalId));
        TypedQuery<Order> ordersTyped = em.createQuery(orders);
        List<Order> ordersList = ordersTyped.getResultList();

        if (ordersList.isEmpty()) {
            return null;
        }
        return ordersList.get(0);
    }

    @Override
    public List<Order> findAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Order> query = cb.createQuery(Order.class);
        Root<Order> order = query.from(Order.class);

        CriteriaQuery<Order> orders = query.select(order);
        TypedQuery<Order> ordersTyped = em.createQuery(orders);
        return ordersTyped.getResultList();
    }
}
