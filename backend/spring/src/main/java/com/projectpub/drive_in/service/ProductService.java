/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.drive_in.service;

import com.projectpub.drive_in._enum.EProductType;
import com.projectpub.drive_in._interface.IProductDao;
import com.projectpub.drive_in._interface.IProductService;
import com.projectpub.drive_in.domain.Order;
import com.projectpub.drive_in.domain.Product;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Jip
 */
@Service
public class ProductService implements IProductService {

    @Autowired
    private IProductDao ProductDaoInstance;

    @Override
    public Product findByInternalId(long internalId) {
        return ProductDaoInstance.findByInternalId(internalId);
    }

    @Override
    public Product create(long externalId, String name, long priceInCents, EProductType category) {
        if (name.isEmpty()) {
            throw new IllegalArgumentException("name may not be empty");
        }

        if (category == null) {
            throw new IllegalArgumentException("category may not be empty");
        }

        if (ProductDaoInstance.findByExternalId(externalId) != null) {
            throw new IllegalArgumentException("product does already exists");
        }

        return ProductDaoInstance.create(new Product(externalId, name, priceInCents, category));
    }

    @Override
    public Product update(long internalId, long externalId, String name, long priceInCents, EProductType productType) {
        if (name.isEmpty()) {
            throw new IllegalArgumentException("name may not be empty");
        }

        if (productType == null) {
            throw new IllegalArgumentException("productType may not be empty");
        }

        if (priceInCents < 0) {
            throw new IllegalArgumentException("price may not be negative");
        }

        Product tempProduct = ProductDaoInstance.findByInternalId(internalId);

        if (tempProduct == null) {
            throw new IllegalArgumentException("Product with internalId does not exists");
        }

        tempProduct.setId(externalId);
        tempProduct.setName(name);
        tempProduct.setPriceInCents(priceInCents);
        tempProduct.setCategory(productType);

        return ProductDaoInstance.update(tempProduct);
    }

    @Override
    public Product findByExternalId(long externalId) {
        return ProductDaoInstance.findByExternalId(externalId);
    }

    @Override
    public List<Product> findAll() {
        return ProductDaoInstance.findAll();
    }
}
