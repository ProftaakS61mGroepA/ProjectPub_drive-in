/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.drive_in._interface;

import com.projectpub.drive_in._enum.EOrderState;
import com.projectpub.drive_in.domain.Order;
import java.util.List;

/**
 *
 * @author Jip
 */
public interface IOrderDao {

    Order create(Order hastag);

    Order update(Order hastag);

    Order findByInternalId(long internalId);

    Order findByExternalId(long externalId);

    List<Order> findAll();
}
