/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.drive_in.dao;

import com.projectpub.drive_in._interface.IOrderDao;
import com.projectpub.drive_in._interface.IProductDao;
import com.projectpub.drive_in.domain.Order;
import com.projectpub.drive_in.domain.Product;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 *
 * @author Jip
 */
@Repository
public class ProductDaoJpa implements IProductDao {

    @PersistenceContext
    EntityManager em;

    @Override
    public Product create(Product product) {
        em.persist(product);
        em.flush();
        return product;
    }

    @Override
    public Product update(Product product) {
        em.merge(product);
        em.flush();
        return product;
    }

    @Override
    public Product findByInternalId(long internalId) {
        return em.find(Product.class, internalId);
    }

    @Override
    public Product findByExternalId(long externalId) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Product> query = cb.createQuery(Product.class);
        Root<Product> product = query.from(Product.class);

        CriteriaQuery<Product> products = query.select(product).where(cb.equal(product.get("id"), externalId));
        TypedQuery<Product> productsTyped = em.createQuery(products);
        List<Product> productList = productsTyped.getResultList();

        if (productList.isEmpty()) {
            return null;
        }
        return productList.get(0);
    }

    @Override
    public List<Product> findAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Product> query = cb.createQuery(Product.class);
        Root<Product> product = query.from(Product.class);

        CriteriaQuery<Product> products = query.select(product);
        TypedQuery<Product> productsTyped = em.createQuery(products);
        return productsTyped.getResultList();
    }
}
