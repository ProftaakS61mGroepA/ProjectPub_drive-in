/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.drive_in._interface;

import com.projectpub.drive_in._enum.EOrderState;
import com.projectpub.drive_in._enum.EProductType;
import com.projectpub.drive_in.domain.Order;
import com.projectpub.drive_in.domain.Product;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jip
 */
public interface IProductService {

    Product create(long externalId, String name, long priceInCents, EProductType productType);

    Product update(long internalId, long externalId, String name, long priceInCents, EProductType productType);

    Product findByInternalId(long internalId);

    Product findByExternalId(long externalId);

    List<Product> findAll();
}
