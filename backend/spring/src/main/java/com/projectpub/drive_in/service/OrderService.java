/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.drive_in.service;

import com.projectpub.drive_in._enum.EOrderState;
import com.projectpub.drive_in._interface.IOrderDao;
import com.projectpub.drive_in._interface.IOrderService;
import com.projectpub.drive_in.domain.Order;
import com.projectpub.drive_in.domain.Product;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Jip
 */
@Service
public class OrderService implements IOrderService {

    @Autowired
    private IOrderDao orderDaoInstance;

    @Override
    public Order create(long externalId, ArrayList<Product> products) {
        if (products == null) {
            throw new IllegalArgumentException("products may not be null");
        }

        if (orderDaoInstance.findByExternalId(externalId) != null) {
            throw new IllegalArgumentException("order does already exists");
        }

        return orderDaoInstance.create(new Order(externalId, products));
    }

    @Override
    public Order update(long internalId, long externalId, ArrayList<Product> products, EOrderState eOrderState) {
        if (products == null) {
            throw new IllegalArgumentException("products may not be null");
        }

        if (eOrderState == null) {
            throw new IllegalArgumentException("eOrderState may not be null");
        }

        Order tempOrder = orderDaoInstance.findByInternalId(internalId);

        if (tempOrder == null) {
            throw new IllegalArgumentException("Order with internalId does not exists");
        }

        tempOrder.setId(externalId);
        tempOrder.setOrderState(eOrderState);
        tempOrder.setProducts(products);

        return orderDaoInstance.update(tempOrder);
    }

    @Override
    public Order findByInternalId(long internalId) {
        return orderDaoInstance.findByInternalId(internalId);
    }

    @Override
    public Order findByExternalId(long externalId) {
        return orderDaoInstance.findByExternalId(externalId);
    }

    @Override
    public List<Order> findAll() {
        return orderDaoInstance.findAll();
    }

    @Override
    public Order onOrderStatusChange(long externalId, EOrderState eOrderState) {
        Order existingOrder = orderDaoInstance.findByExternalId(externalId);

        if (existingOrder == null) {
            throw new IllegalArgumentException("order does not exists");
        }

        if (existingOrder.getOrderState().ordinal() >= eOrderState.ordinal()) {
            throw new IllegalArgumentException("order status is equal or lower");
        }

        existingOrder.setOrderState(eOrderState);

        return orderDaoInstance.update(existingOrder);
    }
}
