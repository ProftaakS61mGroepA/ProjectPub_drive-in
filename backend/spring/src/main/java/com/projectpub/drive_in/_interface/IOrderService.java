/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.drive_in._interface;

import com.projectpub.drive_in._enum.EOrderState;
import com.projectpub.drive_in.domain.Order;
import com.projectpub.drive_in.domain.Product;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jip
 */
public interface IOrderService {

    Order create(long externalId, ArrayList<Product> products);

    Order update(long internalId, long externalId, ArrayList<Product> products, EOrderState eOrderState);

    Order onOrderStatusChange(long externalId, EOrderState eOrderState);

    Order findByInternalId(long internalId);

    Order findByExternalId(long externalId);

    List<Order> findAll();
}
