package com.projectpub.drive_in.controller;

import com.projectpub.drive_in._enum.EOrderState;
import com.projectpub.drive_in._enum.EProductType;
import java.util.logging.Level;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.projectpub.drive_in._interface.IOrderService;
import com.projectpub.drive_in._interface.IProductService;
import com.projectpub.drive_in.domain.Order;
import com.projectpub.drive_in.domain.Product;
import java.util.ArrayList;
import java.util.List;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@RestController
@RequestMapping("/drive-in")
public class OrderController {

    @Autowired
    private IOrderService orderServiceInstance;

    @ExceptionHandler(Exception.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String handleException(Exception e) {
        System.err.println(e.getMessage());
        return e.getMessage();
    }

    @Transactional
    @RequestMapping(value = "/order", method = RequestMethod.POST)
    public Order orderCreate(@RequestBody Order order) throws Exception {
        return orderServiceInstance.create(order.getId(), order.getProducts());
    }

    @Transactional
    @RequestMapping(value = "/order", method = RequestMethod.PUT)
    public Order orderUpdate(@RequestBody Order order) throws Exception {
        return orderServiceInstance.update(order.getInternalId(), order.getId(), order.getProducts(), order.getOrderState());
    }

    @Transactional
    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public List<Order> ordersGet() throws Exception {
        return orderServiceInstance.findAll();
    }

    @Transactional
    @RequestMapping(value = "/onorderstatuschange", method = RequestMethod.PUT)
    public Order onOrderStatusChange(@RequestBody Order order) throws Exception {
        return orderServiceInstance.onOrderStatusChange(order.getId(), order.getOrderState());
    }
}
