/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.drive_in._interface;

import com.projectpub.drive_in.domain.Order;
import com.projectpub.drive_in.domain.Product;
import java.util.List;

/**
 *
 * @author Jip
 */
public interface IProductDao {

    Product create(Product product);

    Product update(Product product);

    Product findByInternalId(long internalId);

    Product findByExternalId(long externalId);

    List<Product> findAll();
}
