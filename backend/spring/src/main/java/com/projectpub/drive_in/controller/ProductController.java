package com.projectpub.drive_in.controller;

import com.projectpub.drive_in._enum.EOrderState;
import java.util.logging.Level;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.projectpub.drive_in._interface.IOrderService;
import com.projectpub.drive_in._interface.IProductService;
import com.projectpub.drive_in.domain.Order;
import com.projectpub.drive_in.domain.Product;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@RestController
@RequestMapping("/drive-in")
public class ProductController {

    @Autowired
    private IProductService productServiceInstance;

    @ExceptionHandler(Exception.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String handleException(Exception e) {
        return e.getMessage();
    }

    @Transactional
    @RequestMapping(value = "/product", method = RequestMethod.POST)
    public Product ProductCreate(@RequestBody Product product) throws Exception {
        return productServiceInstance.create(product.getId(), product.getName(), product.getPriceInCents(), product.getCategory());
    }

    @Transactional
    @RequestMapping(value = "/product", method = RequestMethod.PUT)
    public Product productUpdate(@RequestBody Product product) throws Exception {
        return productServiceInstance.update(product.getInternalId(), product.getId(), product.getName(), product.getPriceInCents(), product.getCategory());
    }

    @Transactional
    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public List<Product> productsGet() throws Exception {
        return productServiceInstance.findAll();
    }
}
